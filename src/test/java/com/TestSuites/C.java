package com.TestSuites;

class P {
	void method1() {
		System.out.println("method 1");
	}

	void method2() {
		System.out.println("parent method 2");
	}
}

class Child extends P {
	void method2() {
		System.out.println("child method2");
	}
}

public class C {
	public static void main(String[] args) {
		P p = new Child();
		p.method2();
	}
}
