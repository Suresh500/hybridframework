package com.TestSuites;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.Base.BaseClass;
import com.utilities.SafeActions;
import com.utilities.Utility;
import freemarker.log.Logger;

public class CogniphiTest extends BaseClass {
	SafeActions safeaction;
	List<String> tcNames = new ArrayList<String>();
	static String tcNum;
	Utility utility = new Utility();
	StringBuffer text = new StringBuffer();
	List<String> resultSet = new ArrayList<>();
	List<String> testCases = new ArrayList<>();
	List<String> lastObject = new ArrayList<>();
	static int num = 0;
	
	static boolean isRowEmpty;
	XSSFRow dataRow;
	static int stepResultCol = 0;
	private static final Logger log = Logger.getLogger(SafeActions.class.getName());
	static int inpCell;
	
	//HSSFRow dataRow ;
	HSSFCell testCell;
	@Test
	public void driveData() throws IOException {

		List<String> header = new ArrayList<>();
		safeaction = new SafeActions(driver);
		String path = reader.get("inputFilePath");
		try {

			fis = new FileInputStream(path);
			FileOutputStream fileOut = new FileOutputStream( new File(reader.get("outputFilePath")));
			workbook = new XSSFWorkbook(fis);
			outWorkbook = new XSSFWorkbook();
			for (int sheets = 0; sheets < workbook.getNumberOfSheets(); sheets++) {

				inputSheet = workbook.getSheetAt(sheets);
				int rowCount = inputSheet.getLastRowNum();
				int colCount = inputSheet.getRow(0).getLastCellNum();

				XSSFSheet sheet = outWorkbook.createSheet(sheets + " Results");

				Font headerFont = outWorkbook.createFont();
				headerFont.setFontHeightInPoints((short) 14);
				headerFont.setColor(IndexedColors.BLUE.getIndex());
				CellStyle headerCellStyle = outWorkbook.createCellStyle();
				headerCellStyle.setFont(headerFont);

				Font result = outWorkbook.createFont();
				result.setColor(IndexedColors.GREEN.getIndex());
				CellStyle resultStyle = outWorkbook.createCellStyle();
				resultStyle.setFont(result);

				Font resultFail = outWorkbook.createFont();
				resultFail.setColor(IndexedColors.RED.getIndex());
				CellStyle resulStyle = outWorkbook.createCellStyle();
				resulStyle.setFont(resultFail);
				XSSFRow headerRow = sheet.createRow(0);
				DataFormatter formatter = new DataFormatter();
				for (int col = 0; col < colCount; col++) {
					String object = formatter.formatCellValue(inputSheet.getRow(0).getCell(col));
					if (object.isEmpty() == false) {
						if (object.contains("TestData")) {
							header.add(object);
							header.add("");
						} else {
							header.add(object);
						}
					}
					if (object.equalsIgnoreCase("Object Repository")) {
						objectRepoCol = col;
					} else if (object.equalsIgnoreCase("Object Type")) {
						objectTypeCol = col;
					} else if (object.equalsIgnoreCase("Object Name")) {
						objectNamecol = col;
					} else if (object.equalsIgnoreCase("TestData") || object.equalsIgnoreCase("TestData1")) {
						testDataCol = col;
					} else if (object.equalsIgnoreCase("Test Case")) {
						testCaseNumCol = col;
					} else if (object.isEmpty() == true) {
						break;
					}
				}

				int incement = 1;
				for (int testdata = testDataCol; testdata < colCount; testdata++) {

					String resultHeader = formatter.formatCellValue(inputSheet.getRow(0).getCell(testdata));
					if (resultHeader.isEmpty() == false) {
						stepResultCol = testdata + incement;
						header.set(stepResultCol, resultHeader + " Result");
						incement++;
					}
				}

				header.add("Test Status");
				header.add("Test Start Time");
				header.add("Test End Time");
				for (int i = 1; i <= rowCount; i++) {
					String value = formatter.formatCellValue(inputSheet.getRow(i).getCell(objectTypeCol));
					testCases.add(formatter.formatCellValue(inputSheet.getRow(i).getCell(0)));
					if (value.isEmpty() == true) {
						testCases.add(i - 1, "End");
						break;
					}
				}
				// To write headings
				for (int i = 0; i < header.size(); i++) {
					XSSFCell cell = headerRow.createCell(i);
					cell.setCellValue(header.get(i));
					cell.setCellStyle(headerCellStyle);
				}
				
				int dataColumns = header.size() - incement - 3;
				int inccol = 1;  
				int inpColInc = 0;
				
				for (int col = testDataCol; col <= dataColumns; col++) {
					int resultSetCell = col + inccol;
					int tcIdRow = 0;
					
					for (int row = 1; row < testCases.size(); row++) {		
												
						tcIdRow = num + 1;	
						String objectTypeData = formatter
								.formatCellValue(inputSheet.getRow(row).getCell(objectTypeCol));
						  if (objectTypeData.equalsIgnoreCase("")) {break;}
						  if(col == testDataCol) {
								 dataRow = sheet.createRow(row);
								}			
						  
						Calendar calender = Calendar.getInstance();
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss:SSS");
						// To print Test case Start time
						if (formatter.formatCellValue(inputSheet.getRow(row).getCell(testCaseNumCol))
								.isEmpty() == false) {							
							Cell cellTime = dataRow.createCell(header.size() - 2);								
							cellTime.setCellValue(dateFormat.format(calender.getTime()));														
						}
						String testStep = formatter.formatCellValue(inputSheet.getRow(row).getCell(objectRepoCol - 1));
						String testData = formatter.formatCellValue(inputSheet.getRow(row).getCell(col));
						String objPath = formatter.formatCellValue(inputSheet.getRow(row).getCell(objectRepoCol));
						
						By objectpath = By.xpath(objPath);
						String objName = formatter.formatCellValue(inputSheet.getRow(row).getCell(objectNamecol));
						if (objectTypeData.equalsIgnoreCase("textfield") || objectTypeData.equalsIgnoreCase("upload")) {
							String status = safeaction.enterTextBoxValue(objectpath, testData, objName, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("button")
								|| objectTypeData.equalsIgnoreCase("checkBox")
								|| objectTypeData.equalsIgnoreCase("radioButton")
								|| objectTypeData.equalsIgnoreCase("link")) {
							String status = safeaction.clickField(objectpath, objName, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("dropdown")) {
							String status = safeaction.selectDropdownOption(objectpath, testData, objName, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("url")) {
							String status = hitURL(testData, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("ImplicitWait")) {
							implicitWait(Integer.parseInt(testData));
						} else if (objectTypeData.equalsIgnoreCase("Explicitwait")) {
							waitUntilElementVisible(objectpath, Integer.parseInt(testData), objName);
						} else if (objectTypeData.equalsIgnoreCase("hardWait")) {
							hardWait(testData);
						} else if (objectTypeData.equalsIgnoreCase("title")) {
							String status = utility.verifyPageTitle(testData, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("labelText")
								|| objectTypeData.equalsIgnoreCase("linkText")
								|| objectTypeData.equalsIgnoreCase("buttonText")
								|| objectTypeData.equalsIgnoreCase("plainText")) {
							String status = safeaction.verifyTextPresence(objPath, testData, objName, objectTypeData,
									testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("TextProperty")) {
							String status = safeaction.verifyElementStyle(objPath, objName, testData, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("attributeText")) {
							String status = safeaction.validateText(objPath, testData, objName, testStep);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("VerifyLinks")) {
							safeaction.verifyAllBrokenLinksOnApplication();
						} else if (objectTypeData.equalsIgnoreCase("alerts")) {
							String status = safeaction.handleAlert(objPath, testStep, objName, testData);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("VerifyImages")) {
							safeaction.verifyAllBrokenImages();
						} else if (objectTypeData.equalsIgnoreCase("verifyFiledownload")) {
							String status = safeaction.verifyDownloadedFile(testData);
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("SwitchTo")) {
							String status = safeaction.handleFrame_Window(objPath, Integer.parseInt(testData));
							text.append(status);
							resultSet.add(status);
						} else if (objectTypeData.equalsIgnoreCase("clear")) {
							safeaction.element(objectpath, objName).clear();
						} else {
							String status = "Fail - You have entered incorrect objectType: " + "'" + objectTypeData
									+ "'";
							log.error("Fail - You have entered incorrect objectType: " + "'" + objectTypeData + "'");
							text.append(status);
							resultSet.add(status);
						}

						String tcName = formatter.formatCellValue(inputSheet.getRow(tcIdRow).getCell(testCaseNumCol));
						
						
						for (int i = 0; i < header.size()-3; i++) {
														
							if (col == testDataCol && i < testDataCol) {
								Cell dataCell = dataRow.createCell(i);								
								String cellData = formatter.formatCellValue(inputSheet.getRow(row).getCell(i));
								if (testCaseNumCol == i && objectTypeData.isEmpty() == false) {
									dataCell.setCellValue(tcName + "-S0" + row);
								} else {
									dataCell.setCellValue(cellData);
								}
							} 
							if(i == col){						
								
								inpCell = i + inpColInc;
								String inpData = formatter.formatCellValue(inputSheet.getRow(row).getCell(i));	
								if(col == testDataCol) {
								XSSFCell inputdataCell = dataRow.createCell(inpCell);
								inputdataCell.setCellValue(inpData);								
																	
								}																																
							}
							
							if (row != 1 && testCases.get(row).isEmpty() == false) {
								if (objectRepoCol - 2 == i) {
									for (int j = row; j > tcIdRow; j--) {
										Cell stepname = sheet.getRow(j).createCell(objectRepoCol - 2);
										stepname.setCellValue(formatter.formatCellValue(
												inputSheet.getRow(tcIdRow).getCell(objectRepoCol - 2)));
									}
								}															
							}

							if (resultSetCell == i) {												
								XSSFCell dataCelll = dataRow.createCell(resultSetCell);																
								dataCelll.setCellValue(text.toString());							
								if (text.toString().equalsIgnoreCase("Pass")) {
									dataCelll.setCellStyle(resultStyle);																	
								} else {
									dataCelll.setCellStyle(resulStyle);																		
								}								
							} 
							//sheet.autoSizeColumn(i);
						} // Column loop to print data
						
						text.delete(0, text.length());

						if (row != 1 && testCases.get(row).isEmpty() == false) {
							XSSFCell cellends = sheet.getRow(tcIdRow).createCell(header.size() - 3);
							for (int k = 0; k < resultSet.size(); k++) {
								if (resultSet.get(k).substring(0, 4).equalsIgnoreCase("Fail")) {
									cellends.setCellValue("Fail");
									cellends.setCellStyle(resulStyle);
									break;
								} else { // has to change
									cellends.setCellValue("Pass");
									cellends.setCellStyle(resultStyle);
								}
							}
							resultSet.clear();
							num = row;
							// To print Test case End time
							Cell cellend = sheet.getRow(tcIdRow).createCell(header.size() - 1);
							cellend.setCellValue(dateFormat.format(calender.getTime()));
						}							
					} // Row for loop
					num = 0;
					inccol++;		
					inpColInc++;
					tcIdRow =0;
																		
				} // column loop
				num = 0;				
				testCases.clear();
				header.clear();					
			} // Sheet loop		
			
			outWorkbook.write(fileOut);	
			fileOut.close();	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// Method
}// Main Class
