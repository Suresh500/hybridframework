package com.TestSuites;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.testng.annotations.Test;

public class NewTest {

	@Test(enabled = false)
	public void f() {
		String a[] = { "e", "f", "g" };
		String b[] = { "a", "b", "c" };
		List list = new ArrayList<>(Arrays.asList(a));
		list.addAll(Arrays.asList(b));
		System.out.println(list);

	}

	@Test(enabled = false)
	public void primeNUmbers() {
		int limit = 100;
		System.out.println("Prime numbers between 1 and " + limit);
		for (int i = 2; i < limit; i++) {
			boolean isPrime = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime)
				System.out.print(i + " ");
		}

	}

	@Test(enabled = true)
	public void duplicateCharaacters() {
		/*
		 * String str = "bvrblnleiufndsnvvvnvn";
		 * 
		 * char[] chars = str.toCharArray(); Map<Character, Integer> map = new
		 * HashMap<>();
		 * 
		 * for (Character ch : chars) { if (map.containsKey(ch)) { map.put(ch,
		 * map.get(ch) + 1); } else { map.put(ch, 1); } } Set<Character> keys =
		 * map.keySet(); for (Character c : keys) { if (map.get(c) > 1) {
		 * System.out.println(c + " " + map.get(c)); } }
		 */

		String str2 = "((((())))";

		char[] ch = str2.toCharArray();

		Map<Character, Integer> map = new HashMap<>();
		for (Character c : ch) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
			} else {
				map.put(c, 1);
			}
		}
		Set<Character> set = map.keySet();
		for (Character cha : set) {
			if (map.get(cha) > 1) {
				System.out.println(cha + "   " + map.get(cha));
			}
		}
	}

	@Test(enabled = false)
	public void sortString() {
		/*
		 * String str = "suresh"; char[] ch = str.toCharArray(); Arrays.sort(ch);
		 * System.out.println(ch);
		 */
		String str = "662366372379973772377563562365265";
		char[] c = str.toCharArray();
		Arrays.sort(c);
		System.out.println(c);
	}

	@Test(enabled = false)
	public void reverseString() {
		/*
		 * String str = "suresh is a good boy";
		 * 
		 * String[] words = str.split(" "); String reversedWords = "";
		 * 
		 * for (int i = 0; i < words.length; i++) { String word = words[i]; String
		 * reversedString = ""; for (int j = word.length() - 1; j >= 0; j--) {
		 * reversedString = reversedString + word.charAt(j); } reversedWords =
		 * reversedWords + reversedString + " "; } System.out.println();
		 * System.out.println(reversedWords);
		 */

		String str = "Suresh Babu ";
		String revString = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			revString = revString + str.charAt(i);
		}
		System.out.println(revString);
	}

	@Test(enabled = false)
	public void reverseNumber() {
		int n = 151, r, temp = 0, q;
		q = n;
		while (q > 0) {
			r = q % 10;
			temp = (temp * 10) + r;
			q = q / 10;
		}
		System.out.println(temp);
		if (temp == n) {
			System.out.println("p");
		} else
			System.out.println("NP");
	}

	@Test(enabled = false)
	public void bigNumber() {
		int[] ar = { 21, 5, 7, 45, 38, 75, 89, 3, 6 };

		Arrays.sort(ar);
		System.out.println(ar[ar.length - 1]);

		int firstBig = 0;
		int secBig = 0;
		for (int num : ar) {
			if (firstBig < num) {
				secBig = firstBig;
				firstBig = num;
			}
		}
		System.out.println("Fisrt " + firstBig + " second " + secBig);
	}

	@Test(enabled = false)
	public void duplicateString() {
		String str = "I am not a good boy i am not bad boy";

		String[] s = str.split(" ");
		// List<String> list = new ArrayList<>(Arrays.asList(s));

		// Set<String> set = new HashSet<>(Arrays.asList(s));

		// System.out.println("Removing duplicates "+set);

		// int count = Collections.frequency(list, "am");
		// System.out.println("Repeated "+count);

		Map<String, Integer> map = new HashMap<>();

		for (String reS : s) {
			if (map.containsKey(reS)) {
				map.put(reS, map.get(reS) + 1);
			} else {
				map.put(reS, 1);
			}
		}
		System.out.println(map);
		Set<String> sett = map.keySet();
		for (String se : sett) {
			if (map.get(se) > 1)
				System.out.println(se + " ---" + map.get(se));
		}

	}

	@Test(enabled = false)
	public void decimalBinary() {
		int n = 8123;
		System.out.println(Integer.toBinaryString(n));

	}

	@Test(enabled = false)
	public void sTRING() {
		String str = "Suresh";
		StringBuffer sb = new StringBuffer(str);
		System.out.println(sb.reverse());
		System.out.println(str.substring(4));
		String str2 = "Madhavi";
		String str3 = str.concat(str2);
		System.out.println(str3.substring(6));
		System.out.println(str.startsWith("Su"));
		System.out.println(str.replaceAll("sh", "BU"));
		System.out.println(str.replaceFirst("esh", "BUf"));
	}

	@Test(enabled = false)
	public void pattern1() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}

	@Test(enabled = false)
	public void pattern2() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 0; j < 5 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 1; j <= 2 * i - 1; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
		for (int i = 4; i >= 1; i--) {
			for (int j = 0; j < 5 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 1; j <= 2 * i - 1; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}

	@Test
	public void method() {
		// bubbleSort();
		// findStringLengthWithoutLengthFunction();
		// findDuplicatesUsingJava8();
		// findDuplicatesInStrging();
		//fibonacciSeries(10);
		findDupicatesUsingFrequency();

	}

	public static Boolean isPrime(int n) {
		if (n <= 1) {
			return false;
		}
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public void primeNumbers(int num) {
		for (int i = 2; i <= num; i++) {
			if (isPrime(i)) {
				System.out.println(i + " ");
			}
		}
	}

	public void fibonacciSeries(int n) {

		int a = 0, b = 0, c = 1;

		for (int i = 0; i < n; i++) {
			a = b;
			b = c;
			c = a + b;
			System.out.println(a);
		}
	}

	public void findStringLengthWithoutLengthFunction() {
		String str = "Suresh";
		int i = 0;
		for (char c : str.toCharArray()) {
			i++;
		}
		System.out.println(i);
	}

	public void bubbleSort() {

		int[] ser = { 23, 65, 7, 30, 2, 9 };
		int temp = 0;
		for (int i = 0; i < ser.length; i++) {
			for (int j = i + 1; j < ser.length; j++) {
				if (ser[i] < ser[j]) {
					temp = ser[i];
					ser[i] = ser[j];
					ser[j] = temp;
				}

			}
			System.out.println(ser[i]);
		}
		Arrays.sort(ser);
		for (int i = 0; i < ser.length; i++) {
			System.out.println(ser[i]);
		}

	}

	public static void findDuplicatesUsingJava8() {
		int[] ser = { 2, 7, 7, 30, 2, 9 };

		// List list = new ArrayList();
		// list.addAll(Arrays.asList());
		// Set<Integer> uniqueElements = new HashSet<>();

		/*
		 * Set<Integer> duplicateElements = Arrays.stream(ser) .filter(i ->
		 * !uniqueElements.add(i)) .boxed() .collect(Collectors.toSet());
		 */
		// System.out.println(duplicateElements);

		Map<Integer, Integer> map = new HashMap<>();

		for (int num : ser) {
			if (map.containsKey(num)) {
				map.put(num, map.get(num) + 1);
			} else {
				map.put(num, 1);
			}
		}
		System.out.println(map);

	}

	public void findDuplicatesInStrging() {
		String str = "suresh";
		String str2 = new String();
		for (int i = 0; i < str.length(); i++) {
			if (str2.indexOf(str.charAt(i)) < 0) {
				str2 += str.charAt(i);
			} else
				System.out.println(str.charAt(i) + " Duplicated");
		}
		System.out.println(str2);
	}

	public void findDuplicateValuesInMapOBject() {
		Map<Integer, String> directory = new HashMap<Integer, String>();
		Map<String, Integer> repeatedEntries = new HashMap<String, Integer>();

		// adding data
		directory.put(1, "john");
		directory.put(2, "michael");
		directory.put(3, "mike");
		directory.put(4, "anna");
		directory.put(5, "julie");
		directory.put(6, "simon");
		directory.put(7, "tim");
		directory.put(8, "ashley");
		directory.put(9, "john");
		directory.put(10, "michael");
		directory.put(11, "mike");
		directory.put(12, "anna");
		directory.put(13, "julie");
		directory.put(14, "simon");
		directory.put(15, "tim");
		directory.put(16, "ashley");

		for (Entry<Integer, String> entry : directory.entrySet()) {
			if (repeatedEntries.containsKey(entry.getValue())) {
				repeatedEntries.put(entry.getValue(), repeatedEntries.get(entry.getValue()) + 1);
			} else {
				repeatedEntries.put(entry.getValue(), 1);
			}
		}
		System.out.println(repeatedEntries);
	}
	
	public static void findDupicatesUsingFrequency() {
		List<String> list = new ArrayList<>();
		
		list.add("Suresh");
		list.add("Madhavi");
		list.add("Suresh");
		list.add("Madhavi");
		list.add("Suresh");
		list.add("Harshhitha");
		list.add("Lohhithaya");
		list.add("Suresh");
		list.add("Harshhitha");
		list.add("Harshhitha");
		list.add("Lohhithaya");
		list.add("Lohhithaya");
		list.add("Amma");
		list.add("Lohhithaya");
		
		Set<String> set = new HashSet<>(list);
		for(String data: set) {
			int count = Collections.frequency(list, data);
			System.out.println(data+"  "+count);
		}
			
	}
}
