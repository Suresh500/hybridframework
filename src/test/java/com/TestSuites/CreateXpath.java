package com.TestSuites;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;

public class CreateXpath {

	public CreateXpath() {
		
	}
	
	public static By id = By.xpath("//*[id='{0}']");
	
	public String createXpath(String str, Object ...args) {
		for (int i = 0; i < args.length; i++) {
			str = str.replace("{"+i+"}", (CharSequence)args[i]);
		}
		
		return str;
	}

	public static void main(String[] args) {
		Map<Integer, String> directory=new HashMap<Integer, String>();
        Map<String, Integer> repeatedEntries=new HashMap<String, Integer>();

        // adding data
        directory.put(1,"john");
        directory.put(2,"michael");
        directory.put(3,"mike");
        directory.put(4,"anna");
        directory.put(5,"julie");
        directory.put(6,"simon");
        directory.put(7,"tim");
        directory.put(8,"ashley");
        directory.put(9,"john");
        directory.put(10,"michael");
        directory.put(11,"mike");
        directory.put(12,"anna");
        directory.put(13,"julie");
        directory.put(14,"simon");
        directory.put(15,"tim");
        directory.put(16,"ashley");

        /*for(int i=1;i<=directory.size();i++) {
           String result=directory.get(i);
           for(int j=1;j<=directory.size();j++) {
              if(j!=i && result==directory.get(j) &&j<i) {
                 repeatedEntries.put(j, result);
              }
           }
           System.out.println(result);
        }
        for(Entry<Integer, String> entry : repeatedEntries.entrySet()) {
           System.out.println("repeated "+entry.getValue());
           System.out.println(repeatedEntries);
        }*/
   for(Entry<Integer, String> entry : directory.entrySet()) {
	   if(repeatedEntries.containsKey(entry.getValue())) {
		   repeatedEntries.put(entry.getValue(), repeatedEntries.get(entry.getValue())+1);
	   }
	   else {
		   repeatedEntries.put(entry.getValue(), 1);
	   }
   }
   System.out.println(repeatedEntries);


	}

}
