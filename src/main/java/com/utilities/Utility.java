package com.utilities;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.Base.BaseClass;
import com.aventstack.extentreports.Status;

public class Utility extends BaseClass {

	SafeActions safeAct = new SafeActions(driver);
	private static final Logger log = Logger.getLogger(Utility.class.getName());

	public String verifyPageTitle(String titleName, String testStep) {
		
		String title = driver.getTitle();
		if (title.equals(titleName)) {
			log.info(titleName + " Verified Successfully as title");
			//test.log(Status.PASS, testStep + ":: Verified Successfully");
			result ="Pass";
		} else {
			//test.log(Status.FAIL, "Title missmatched : Please provide a valid input or Wait time, because Actual is " + title + " But expected is " + titleName);
			log.info("Title missmatched : Actual is " + title + " But expected is " + titleName);
			//Assert.fail();
			result = "Fail - Title missmatched : Actual is " + title + " But expected is " + titleName;
		}
		return result;
	}// Method to verify title of the page

	public void validateReport(List<String> appNames, List<String> dbNames)
			throws ClassNotFoundException, SQLException {
		for (int i = 0; i < dbNames.size(); i++) {
			for (int j = 0; j < appNames.size(); j++) {
				if (dbNames.get(i).equalsIgnoreCase((appNames.get(j))))
					dbNames.set(i, "");
			}
		}
		System.err.println(dbNames + " Not showing in the application");
		Assert.assertTrue(dbNames.isEmpty(), "Data miss match exist in the report");
	}// Method to validate One month report
	
	

}// Main Class
