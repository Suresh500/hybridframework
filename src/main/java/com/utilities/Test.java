package com.utilities;

import java.io.IOException;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;



public class Test {
				
	public static void main(String[] args) throws IOException {				
		TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
        testng.setTestClasses(new Class[] { CogniphiTest.class });
        testng.addListener(tla);
        testng.run();
		
		
	}						
}
