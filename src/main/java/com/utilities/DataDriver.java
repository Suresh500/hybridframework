package com.utilities;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import com.Base.BaseClass;

/**
 * Process data, test appication's UI & gives report.
 *
 * @author Suresh BM, 7th Jan, 2019
 */
public class DataDriver extends BaseClass {
	SafeActions safeaction;
	List<String> tcNames = new ArrayList<String>();
	static String tcNum;
	Utility utility = new Utility();

	public void driveData(String testCaseId) {
		List<String> header = new ArrayList<>();
		safeaction = new SafeActions(driver);
		String path = reader.get("inputFilePath");
		try {
			fis = new FileInputStream(path);
			workbook = new XSSFWorkbook(fis);
			// Iterator<XSSFSheet> sheetIterator = workbook.iterator();
			// while (sheetIterator.hasNext()) {
			inputSheet = workbook.getSheetAt(0);
			int rowCount = inputSheet.getLastRowNum();
			int colCount = inputSheet.getRow(0).getLastCellNum();
			for (int col = 0; col < colCount; col++) {
				String object = inputSheet.getRow(0).getCell(col).getStringCellValue();
				if (object.isEmpty() == false)
					header.add(object);
				if (object.equalsIgnoreCase("Object Repository")) {
					objectRepoCol = col;
				} else if (object.equalsIgnoreCase("Object Type")) {
					objectTypeCol = col;
				} else if (object.equalsIgnoreCase("Object Name")) {
					objectNamecol = col;
				} else if (object.contains("Test Data")) {
					testDataCol = col;
				} else if (object.equalsIgnoreCase("Test Case")) {
					testCaseNumCol = col;
				} else if (object.isEmpty() == true) {
					break;
				}
			} // Column loop
			System.out.println(header);
			for (int row = 1; row <= rowCount; row++) {
				tcNum = inputSheet.getRow(row).getCell(testCaseNumCol).getStringCellValue();
				if (tcNum.equals(testCaseId)) {
					rownum = row;
					break;
				}
			}
			// for (int col = objectNamecol + 1; col <= header.size() - 1; col++) {
			for (int row = rownum; row <= rowCount; row++) {
				DataFormatter formatter = new DataFormatter();
				String objectTypeData = inputSheet.getRow(row).getCell(objectTypeCol)
						.getStringCellValue();
				if (objectTypeData.isEmpty() == true) {
					break;
				}
				String testStep = inputSheet.getRow(row).getCell(objectRepoCol - 1)
						.getStringCellValue();
				String testData = formatter.formatCellValue(inputSheet.getRow(row).getCell(testDataCol));
				String objPath = formatter
						.formatCellValue(inputSheet.getRow(row).getCell(objectRepoCol));
				By objectpath = By.xpath(objPath);
				String objName = inputSheet.getRow(row).getCell(objectNamecol)
						.getStringCellValue();

				if (objectTypeData.equalsIgnoreCase("textField") || objectTypeData.equalsIgnoreCase("upload")) {
					safeaction.enterTextBoxValue(objectpath, testData, objName, testStep);
				} else if (objectTypeData.equalsIgnoreCase("button") || objectTypeData.equalsIgnoreCase("checkBox")
						|| objectTypeData.equalsIgnoreCase("radioButton") || objectTypeData.equalsIgnoreCase("link")) {
					safeaction.clickField(objectpath, objName, testStep);
				} else if (objectTypeData.equalsIgnoreCase("dropdown")) {
					safeaction.selectDropdownOption(objectpath, testData, objName, testStep);
				} else if (objectTypeData.equalsIgnoreCase("url")) {
					hitURL(testData, testStep);
				}

				else if (objectTypeData.equalsIgnoreCase("wait")) {
					implicitWait(Integer.parseInt(testData));
				} else if (objectTypeData.equalsIgnoreCase("explicitwait")) {
					waitUntilElementVisible(objectpath, Integer.parseInt(testData), objName);
				} else if (objectTypeData.equalsIgnoreCase("hardWait")) {
					hardWait(testData);
				} else if (objectTypeData.equalsIgnoreCase("title")) {
					utility.verifyPageTitle(testData, testStep);
				} else if (objectTypeData.equalsIgnoreCase("labelText")
						|| objectTypeData.equalsIgnoreCase("linkText")) {
					safeaction.verifyTextPresence(objPath, testData, objName, objectTypeData, testStep);
				} else if (objectTypeData.equalsIgnoreCase("textProperty")) {
					safeaction.verifyElementStyle(objPath, objName, testData, testStep);
				} else if (objectTypeData.equalsIgnoreCase("text")) {
					safeaction.validateText(objPath, testData, objName, testStep);
				} else if (objectTypeData.equalsIgnoreCase("verifyLinks")) {
					safeaction.verifyAllBrokenLinksOnApplication();
				} else if (objectTypeData.equalsIgnoreCase("alerts")) {
					safeaction.handleAlert(objPath, testStep, objName, testData);
				} else if (objectTypeData.equalsIgnoreCase("verifyImages")) {
					safeaction.verifyAllBrokenImages();
				} else if (objectTypeData.equalsIgnoreCase("verifyFiledownload")) {
					safeaction.verifyDownloadedFile(testData);
				} else if (objectTypeData.equalsIgnoreCase("switchTo")) {
					safeaction.handleFrame_Window(objPath, Integer.parseInt(testData));
				}

			} // Row for loop
			// } // column
			// } // While loop
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// Method
}// Main Class
