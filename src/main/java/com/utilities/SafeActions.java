package com.utilities;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.Base.BaseClass;
import com.aventstack.extentreports.Status;
import freemarker.log.Logger;

public class SafeActions extends BaseClass {
	WebDriver driver;
	static Boolean condition;
	static WebElement element;
	static String text;

	private static final Logger log = Logger.getLogger(SafeActions.class.getName());

	public SafeActions(WebDriver driver) {
		this.driver = driver;
	}

	SoftAssert sa = new SoftAssert();

	// Reusable Function
	public WebElement element(By locator, String elementName) {
		try {
			element = driver.findElement(locator);
		} catch (StaleElementReferenceException e) {
			test.log(Status.FAIL,
					elementName + " is not attached to the page document - StaleElementReferenceException");
		} catch (NoSuchElementException e) {
			test.log(Status.FAIL, elementName + " was not found in DOM - NoSuchElementException");
		} catch (Exception e) {
			test.log(Status.FAIL, elementName + " was not found on the web page");
		}
		return element;
	}

	// Method to Click on the element -1
	public String clickField(By locator, String elementName, String testStep) {
		try {
			driver.findElement(locator).click();
			log.info("Driver clicked on " + elementName);			
			result = "Pass";
		} catch (StaleElementReferenceException e) {	
			log.info("Fail - StaleElementReferenceException - Use page refresh or explicit wait");
			result = "Fail - StaleElementReferenceException - Use page refresh or explicit wait";
		} catch (NoSuchElementException e) {			
			log.info("Fail - NoSuchElementException - Please provide implicit wait or hard wait");
			result = "Fail - NoSuchElementException - Please provide implicit wait or hard wait";
		} catch (NullPointerException e) {	
			log.info("Fail - NullPointerException - invalid xpath");
			result = "Fail - NullPointerException - invalid xpath";
		} catch (WebDriverException e) {			
			log.info("Fail - WebDriverException - Take correct path or Use explicict wait before click");
			result = "Fail - WebDriverException - Take correct path or Use explicict wait before click";
		} catch (Exception e) {
			log.info("Exception - Please provide wait time or valid xpath");
			result = "Exception - Please provide wait time or valid xpath";
		}
		return result;
	}

	// Method to send text into the text box -2
	public String enterTextBoxValue(By locator, String data, String elementName, String testStep) {

		try {
			driver.findElement(locator).sendKeys(data);
			result = "Pass";
			log.info(data + " has been entered in " + elementName + " field");
		} catch (StaleElementReferenceException e) {
			log.info("Fail - StaleElementReferenceException");
			result = "Fail - StaleElementReferenceException";
		} catch (NoSuchElementException e) {
			log.info("Fail - NoSuchElementException");
			result = "Fail - NoSuchElementException";
		} catch (WebDriverException e) {
			log.info("Fail - WebDriverException");
			result = "Fail - WebDriverException";
		} catch (Exception e) {
			log.info("Exception - Please provide wait time or valid xpath");
			result = "Exception - Please provide wait time or valid xpath";
		}
		return result;
	}

	// Methods to select value from drop down -3
	public String selectDropdownOption(By locator, String value, String elementName,
			String testStep) {
		String text = elementName;
		String elementNam = text.split(",")[0];
		String elementType = text.split(",")[1].trim();
		try {
			Select dropdown = new Select(driver.findElement(locator));

			if (elementType.equalsIgnoreCase("text")) {
				dropdown.selectByVisibleText(value);
				log.info(elementNam+" selected in the dropdown");
				result = "Pass";
			} else if (elementType.equalsIgnoreCase("value")) {
				dropdown.selectByValue(value);
				log.info(elementNam+" selected in the dropdown");
				result = "Pass";
			} else if (elementType.equalsIgnoreCase("index")) {
				dropdown.selectByIndex(Integer.parseInt(value));
				log.info(elementNam+" selected in the dropdown");
				result = "Pass";
			}
			else if (elementType.equalsIgnoreCase("desel_text")) {
				dropdown.deselectByVisibleText(value);
				result = "Pass";
			}
			else if (elementType.equalsIgnoreCase("desel_value")) {
				dropdown.deselectByValue(value);
				result = "Pass";
			}
			else if (elementType.equalsIgnoreCase("desel_index")) {
				dropdown.deselectByIndex(Integer.parseInt(value));
				result = "Pass";
			}
			else if (elementType.equalsIgnoreCase("is_Mul")) {
				dropdown.isMultiple();
				result = "Pass";
			}			
		} catch (StaleElementReferenceException e) {
			log.info("Fail - StaleElementReferenceException, Please use explicit wait before select");
			result = "Fail - StaleElementReferenceException, Please use explicit wait before select";
		} catch (NoSuchElementException e) {
			log.info("Fail - NoSuchElementException, Please provide wait time & try");
			result = "Fail - NoSuchElementException, Please provide wait time & try";
		} catch (WebDriverException e) {			
			log.info("Fail - WebDriverException");
			result = "Fail - WebDriverException";
		} catch (Exception e) {
			log.info("Fail - Exception, Please provide wait time & try");
			result = "Fail - Exception, Please provide wait time & try";
		}
		return result;

	}

	// Method to collect all elements which having same path but different text -4
	public List<String> collectAllElementsData(By locator, String elementName) {
		List<String> data = new ArrayList<>();
		List<WebElement> ertNames = driver.findElements(locator);
		for (WebElement name : ertNames) {
			data.add(name.getText());
		}
		return data;
	}

	// To collect elements which having same tag & attribute but different text -5
	public List<String> collectAllElementsData(String tagName, String attributeName) {
		List<String> data = new ArrayList<>();
		List<WebElement> ertNames = driver.findElements(By.tagName(tagName));
		for (WebElement name : ertNames) {
			data.add(name.getAttribute(attributeName));
		}
		return data;
	}

	static List<String> data;

	public String validateText(String instruction, String testData, String elementName, String testStep) {		
		data = new ArrayList<>();
		String text = instruction;		
		String tag = text.split(",")[0];
		String attribute = text.split(",")[1].trim();		
		if(tag.contains("/")) {					
				data.add(element(By.xpath(tag), elementName).getAttribute(attribute));								
						}else {
			for (WebElement element : driver.findElements(By.tagName(tag))) {
				data.add(element.getAttribute(attribute));
			}	
		}
		if (data.contains(testData)) {
			log.info(testData+" Text verified successfully");
			result = "Pass";
		} else {
			log.error("Failed due to Expected was: " + testData + " but available is: " + data);
			result = "Failed due to Expected was: " + testData + " but available is: " + data;
		}
		return result;
	}

	// Method to validate element on the web page -7
	public Boolean checkElementDispaly(By loc, String elementName) {
		condition = element(loc, elementName).isDisplayed();
		return condition;
	}

	public Boolean checkELementEnable(By loc, String elementName) {
		condition = element(loc, elementName).isEnabled();
		return condition;
	}

	public Boolean checkElementSelection(By loc, String elementName) {
		condition = element(loc, elementName).isSelected();
		return condition;
	}

	// Method to switch to frame -8
	public String enterTOFrame(String loc) {
		try {
			driver.switchTo().frame(loc);
			result = "Pass";
		} catch (NoSuchFrameException e) {
			// Assert.fail(" Frame has not identified on the page");
			result = "Fail - NoSuchFrameException";
		}
		return result;
	}

	// Method to come out from the frame --
	public void switchBackFromFrame() {
		try {
			driver.switchTo().defaultContent();
		} catch (NoSuchFrameException e) {
			Assert.fail(" Frame has not identified on the page");
		}
	}

	// To handle multiple windows at a time --
	public String handleMultipleWindows(int windowId) {
		try {
			Set<String> multiWindo = driver.getWindowHandles();
			Object[] obj = multiWindo.toArray();
			driver.switchTo().window(obj[windowId].toString());
			result = "Pass";
		} catch (NoSuchWindowException e) {
			result = "Fail - NoSuchWindowException";
		}
		return result;
	}

	public String handleFrame_Window(String path, int windowId) {
		if (path.isEmpty() == true)
			result = handleMultipleWindows(windowId);
		else
			result = new SafeActions(driver).enterTOFrame(path);
		return result;
	}

	// Method to accept alert -10
	public void acceptAlert() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(reader.get("mediumWait")));
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			e.getMessage();
		}
	}

	// Method to Cancel alert -11
	public void dismissAlert() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(reader.get("mediumWait")));
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
			e.getMessage();
		}
	}

	public String handleAlert(String waitTime, String testStep, String action, String testData) {
		WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(waitTime));
		wait.until(ExpectedConditions.alertIsPresent());
		try {
			if (action.equalsIgnoreCase("accept")) {
				driver.switchTo().alert().accept();
				test.log(Status.PASS, testStep);
				result = "Pass";
			} else if (action.equalsIgnoreCase("cancel")) {
				driver.switchTo().alert().dismiss();
				test.log(Status.PASS, testStep);
				result = "Pass";
			} else if (action.equalsIgnoreCase("validate")) {
				String text = driver.switchTo().alert().getText();
				if (text.equals(testData)) {
					test.log(Status.PASS, testStep);
					result = "Pass";
				} else {
					test.log(Status.FAIL, "Test data not matching with alert text");
					result = "Fail - Test data not matching with alert text";
				}
			}
		} catch (NoAlertPresentException e) {
			test.log(Status.FAIL, "Alert has not generated");
			result = "Fail - NoAlertPresentException";
		}
		return result;
	}

	// Method to validate Error alert -12
	public void validateAlert(String exptdAlertContent, String msgIfFailed) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(reader.get("mediumWait")));
			wait.until(ExpectedConditions.alertIsPresent());
			String text = driver.switchTo().alert().getText();
			Assert.assertEquals(text, exptdAlertContent, msgIfFailed);
		} catch (NoAlertPresentException e) {
			e.getMessage();
		}
	}

	// To mouse hover on the element -13
	public void mouseHoverToElement(By loc, String elementName) {
		Actions act = new Actions(driver);
		act.moveToElement(element(loc, elementName)).build().perform();
	}

	// To double click on element -14
	public void doubleClickOnElement(By loc, String elementName) {
		Actions act = new Actions(driver);
		act.doubleClick(element(loc, elementName)).build().perform();
	}

	// Method to perform right click on the element -15
	public void rightClickOnElement(By loc, String elementName) {
		Actions act = new Actions(driver);
		act.contextClick(element(loc, elementName)).build().perform();
	}

	// Method to drag and drop the element -16
	public void dragAndDrop(By sourceLoc, String sourceElementName, By targetLoc, String targetElementName) {
		Actions act = new Actions(driver);
		act.dragAndDrop(element(sourceLoc, sourceElementName), element(targetLoc, targetElementName)).build().perform();
	}

	// Method to collect data from the single element -17
	public String collectSingleElementData(By loc, String elementName) {
		text = element(loc, elementName).getText();
		return text;
	}

	// Method to click on element when WebDriver click not worked -18
	public void jsClick(By loc, String elementName) {
		WebElement element = element(loc, elementName);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	}

	// Method to click on selected date -19
	public void selectDate(By dateWidgetPath, String date, String datePicker) {
		WebElement dateWidget = element(dateWidgetPath, datePicker);
		List<WebElement> columns = dateWidget.findElements(By.tagName("td"));
		for (WebElement cell : columns) {
			if (cell.getText().equals(date)) {
				cell.findElement(By.linkText(date)).click();
				break;
			}
		}
	}

	// Method to Validate downloaded file -20
	public boolean isFileDownloaded_Ext(String dirPath, String extention) throws FileNotFoundException {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 1; i < files.length; i++) {
			if (files[i].getName().contains(extention)) {
				flag = true;
			}
		}
		return flag;
	}

	// To handle two windows --21
	public void handleMultipleWindows() {
		String currentWindow = driver.getWindowHandle();
		Set<String> multiWindo = driver.getWindowHandles();
		Iterator<String> itr = multiWindo.iterator();
		try {
			while (itr.hasNext()) {
				if (!currentWindow.equals(itr.next())) {
					driver.switchTo().window(itr.next());
					System.out.println("Naviagated window title is" + driver.switchTo().window(itr.next()).getTitle());
				}
			}
		} catch (NoSuchWindowException e) {
			e.printStackTrace();
		}
	}

	// To take element screenshot --23
	WebElement elementScreen;

	public void takeElementScreenShot(String locator, String elementName, String objectType) {

		try {
			if (objectType.equalsIgnoreCase("linkText"))
				elementScreen = driver.findElement(By.xpath(locator));
			else if (objectType.equalsIgnoreCase("labelText"))
				elementScreen = driver.findElement(By.cssSelector(locator));
			File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			int elementHight = elementScreen.getSize().getHeight();
			int elementWidth = elementScreen.getSize().getWidth();
			Point point = elementScreen.getLocation();
			int xCod = point.getX();
			int yCod = point.getY();
			BufferedImage img = ImageIO.read(srcFile);
			BufferedImage destination = img.getSubimage(xCod, yCod, elementWidth, elementHight);
			ImageIO.write(destination, "png", srcFile);
			FileUtils.copyFile(srcFile, new File(reader.get("screenshot") + elementName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	/*
	 * To check element properties like background-color, color, font, border,
	 * border-top, margin, margin-top, padding, padding-top, list-style, outline,
	 * pause, cue-24
	 */
	public String verifyElementStyle(String locator, String elementName, String elementProperty, String testStep) {
		String text = elementProperty;
		String textProperty = text.split(",")[0];
		String expectedProperty = text.split(",")[1].trim();
		String actualProperty = element(By.xpath(locator), elementName).getCssValue(textProperty);
		if (actualProperty.equals(expectedProperty)) {
			// test.log(Status.PASS, testStep);
			result = "Pass";
		} else {
			new SafeActions(driver).takeElementScreenShot(locator, elementName, "linkText");
			// test.log(Status.FAIL, testStep + " :.....: Property not meeting client
			// requirement");
			result = "Fail " + testStep + " :.....: Property not meeting client requirement";
		}
		return result;
	}

	public void verifyBrokenLink(String url) throws MalformedURLException {
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			
			connection.setConnectTimeout(10000);
			connection.connect();
			if (connection.getResponseCode() == 200) {
				System.out.println(connection.getResponseMessage());
				test.log(Status.PASS,
						url + " has been working properly" + " HTTP Status: " + connection.getResponseMessage());
			} else {
				System.out.println(connection.getResponseMessage());
				test.log(Status.ERROR, url + " has been broken");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void verifyAllBrokenLinksOnApplication() throws MalformedURLException {
		for (WebElement element : driver.findElements(By.tagName("a"))) {
			verifyBrokenLink(element.getAttribute("href"));
		}
	}

	static String textVerif = "";
	static Boolean Condition = false;

	public String verifyTextPresence(String locator, String testData, String elementName, String objectType,
			String testStep) {
		try {
			if (objectType.equalsIgnoreCase("link Text")) {
				textVerif = element(By.xpath(locator), elementName).getAttribute("innerHTML");
			} else if (objectType.equalsIgnoreCase("label Text")) {
				textVerif = element(By.cssSelector(locator), elementName).getText();
			} else if (objectType.equalsIgnoreCase("button Text") || objectType.equalsIgnoreCase("plain Text")) {
				textVerif = element(By.xpath(locator), elementName).getText();
			}
			if (testData.isEmpty() || textVerif.isEmpty()) {
				log.error(
						"Fail - to read data from page or You forgot to mention test data. \nPlease give valid xpath");
				result = "Fail - to read data from page or You forgot to mention test data. \nPlease give valid xpath";
			} else if (textVerif.equals(testData)) {
				log.info(testData + " Text verified successfully");
				result = "Pass";
			} else {
				log.error("Fail - Expected is: " + testData + " But available is: " + textVerif);
				result = "Fail - Expected is: " + testData + " But available is: " + textVerif;
			}

		} catch (NoSuchElementException e) {
			//takeElementScreenShot(locator, elementName, objectType);
			result = "Fail - NoSuchElementException";
		} catch (StaleElementReferenceException e) {
			//takeElementScreenShot(locator, elementName, objectType);
			result = "Fail - StaleElementReferenceException";
		} catch (Exception e) {
			e.printStackTrace();
			result = "Fail - Please provide valid input or Set wait time";
		}
		return result;
	}

	public void verifyImage(WebElement element) throws ClientProtocolException, IOException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(element.getAttribute("src"));
		HttpResponse response = client.execute(request);
		if (response.getStatusLine().getStatusCode() == 200) {
			test.log(Status.PASS, text + element.getAttribute("src") + " --Status code: " + 200);
		} else {
			test.log(Status.FAIL, text + element.getAttribute("src") + " has been broken");
		}
	}

	public void verifyAllBrokenImages() {
		try {
			for (WebElement element : driver.findElements(By.tagName("img"))) {
				verifyImage(element);
			}
		} catch (Exception e) {

		}
	}

	public String verifyDownloadedFile(String fileName) {

		File files = new File(reader.get("downloadPath"));
		for (File file : files.listFiles()) {
			if (file.getName().equals(fileName)) {
				test.log(Status.PASS, fileName + " has been successfully downloaded into folder");
				result = "Pass";
			} else {
				test.log(Status.ERROR, fileName + " not found in the specified folder");
				result = "Fail " + fileName + " not found in the specified folder";
			}
		}
		return result;
	}

}// Main Class
