package com.Base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.utilities.ConfigReader;
import freemarker.log.Logger;

/**
 * Parent class for all classes in the framework
 *
 * @author Suresh BM, 7th Jan, 2019
 */

public class BaseClass {

	public static WebDriver driver;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	public int objectRepoCol;
	public int objectTypeCol;
	public int objectNamecol;
	public int testCaseNumCol;
	public int testDataCol;
	public static int rownum;
	protected FileInputStream fis;
	protected XSSFWorkbook workbook;
	protected static XSSFSheet inputSheet;
	protected static String result;
	private static final Logger log = Logger.getLogger(BaseClass.class.getName());
	public static ConfigReader reader = new ConfigReader();
	public static FileOutputStream fileOut;
	protected static XSSFWorkbook outWorkbook;
	protected static XSSFSheet sheet;
	/*static {		
		htmlReporter = new ExtentHtmlReporter(reader.get("reports") + ".html");		
		extent = new ExtentReports();
	}*/

	/*public void getResult(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("C:\\Users\\121\\Desktop\\screenShots\\" + result.getName() + ".png"));
		}
	}*/

	@BeforeTest
	// CrossBrowser Testing
	/*
	 * @Parameters("browsers") public static void beforeTest(String browser) {
	 * openBrowser(browser); }
	 */

	public static void beforeTest() throws FileNotFoundException {
		/*extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Environment", "QA");
		htmlReporter.config().setDocumentTitle("Test Report");
		htmlReporter.config().setReportName("Automation Test Result");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.STANDARD);*/
		String browser = reader.get("selectbrowser");
		openBrowser(browser);
	}

	@BeforeMethod
	public void beforeMethod(Method result) {
	//	test = extent.createTest(result.getName());
		// test.log(Status.INFO, result.getName());
	}

	/*@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		getResult(result);
	}*/

	/*
	 * @BeforeClass public void beforeClass() { // hitURL(reader.get("TestURL")); }
	 * 
	 * @AfterClass public void toEndTest() { }
	 */

	@AfterTest
	public void afterTest() throws InterruptedException, IOException {
		//extent.flush();
		//driver.get(reader.get("reportDisplayUrl"));	
		driver.quit();
	}

	// To open Chrome Browser-
	static void launchChrome() {
		Map<String, Object> chromePreferences = new Hashtable<String, Object>();
		chromePreferences.put("profile.default_content_settings.popups", 0);
		chromePreferences.put("download.default_directory", reader.get("downloadPath"));
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("prefs", chromePreferences);
		chromeOptions.addArguments("--disable-notifications");
		chromeOptions.addArguments("disable-infobars");
		chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		chromeOptions.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		// WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver(chromeOptions);
		log.info("chrome browser Launched");
		implicitWait(10);
	}

	// To open firefox -

	static void launchFirefox() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.dir", "D:\\DriverDownloads");
		profile.setPreference("browser.download.folderList", Integer.parseInt(reader.get("saveToSpecific")));
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;");
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("pdfjs.disabled", true);
		FirefoxOptions options = new FirefoxOptions();
		options.setProfile(profile);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, false);
		options.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver(options);
		log.info("Firefox browser Launched");
	}

	// To launch IE -
	static void launchIEBrowser() {
		System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		log.info("IE browser Launched");
	}

	// To launch PhantomJsDriver & to do headless Browser testing -
	static void launchPhantomJS() {
		File src = new File("D:\\Project\\Library\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		System.setProperty("phantomjs.binary.path", src.getAbsolutePath());
		//driver = new PhantomJSDriver();
	}

	// To launch Opera browser -
	static void launchOpera() {
		System.setProperty("webdriver.opera.driver",
				"D:\\Project\\Library\\operadriver_win64\\operadriver_win64\\operadriver.exe");
		driver = new OperaDriver();
	}

	// To enter url in the browser -
	public String hitURL(String url, String testStep) {
		try {
			driver.get(url);
			log.info("URL Entered");
			result = "Pass";
		} catch (WebDriverException e) {
			log.info("URL not found & please check your mentioned URL once");
			result = "Fail - URL not found & please check your mentioned URL once";
			driver.quit();
		}
		return result;
	}

	// Maximize the page -
	static void maximise() {
		driver.manage().window().maximize();
	}

	// Page scroll down-
	public void scrollDownToEnd() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	// Page Refresh -
	public void refreshThePage() {
		driver.navigate().refresh();
	}

	// Page scroll Up-
	public void scrollUpToStart() {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_UP).build().perform();
	}

	// Scroll Offsets -
	public void scrollWithOffsets(int horizontalOffset, int verticalOffset) {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(" + horizontalOffset + "," + verticalOffset + ")");
	}

	// explicitWait -
	public void waitUntilElementVisible(By locator, int timeOut, String text) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			if (text.equalsIgnoreCase("presence"))
				wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			else if (text.equalsIgnoreCase("visible"))
				wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			else if (text.equalsIgnoreCase("toBeClick"))
				wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (TimeoutException te) {
			te.printStackTrace();
		}
	}

	// implicitWait -s
	public static void implicitWait(int timeout) {
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}

	// Wait until page load -
	public void waitUntilPageLoad(int timeout) {
		driver.manage().timeouts().pageLoadTimeout(timeout, TimeUnit.SECONDS);
	}

	// Wait for Script -
	public void waitForScript(int timeout) {
		driver.manage().timeouts().setScriptTimeout(timeout, TimeUnit.SECONDS);

	}

	// threadStop explicitWait -
	public static void hardWait(String timeout) {
		try {
			Thread.sleep(Integer.parseInt(timeout));
		} catch (InterruptedException i) {
			i.printStackTrace();
			Assert.fail();
		}
	}

	static void openBrowser(String browser) throws FileNotFoundException {
		String path = System.getProperty("user.dir") + "/log4j.properties";
		PropertyConfigurator.configure(path);
		if (browser.equalsIgnoreCase("chrome")) {
			launchChrome();
			maximise();
		} else if (browser.equalsIgnoreCase("firefox")) {
			launchFirefox();
			maximise();
		} else if (browser.equalsIgnoreCase("ie")) {
			launchIEBrowser();
			maximise();
		} else if (browser.equalsIgnoreCase("opera")) {
			launchOpera();
			maximise();
		} else if (browser.equalsIgnoreCase("PhantomJs")) {
			launchPhantomJS();
			maximise();
		}
	}

}// Main Class
