package com.pages;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;

import com.Base.BaseClass;
import com.utilities.SafeActions;

public class LoginPage extends BaseClass {
	SafeActions action ;

	// Elements regarding Login Page
	By userName = By.id("username");
	By password = By.id("password");
	By loginButton = By.xpath("/html/body/div[1]/div/div[2]/form/input[3]");

	// Functions regarding Login page
	public void logIn() {
		action = new SafeActions(driver);
		action.enterTextBoxValue(userName, reader.get("UserName"), "UserName", "");
		action.enterTextBoxValue(password, reader.get("Password"), "Password", "");
		action.clickField(loginButton, "LoginButton", "");
	}
	
	public void logIn(String urls, String userNames, String passwords) {
		action = new SafeActions(driver);
		action.enterTextBoxValue(userName, reader.get(userNames), "UserName", "");
		action.enterTextBoxValue(password, reader.get(passwords), "Password", "");
		action.clickField(loginButton, "LoginButton", "");
	}
	
	@DataProvider(name = "loginInputs")
	public Object[][] getData(){
		Object[][] data = new Object[2][3];
		
			data[0][0] = "http://52.66.98.236/muthoot/login.php";
			data[0][1] = "";
			data[0][2] = "";
			
			data[1][0] = "";
			data[1][1] = "";
			data[1][2] = "";
			
		return data;
	}
	
}
